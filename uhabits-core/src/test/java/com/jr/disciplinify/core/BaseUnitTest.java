/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.core;

import android.support.annotation.NonNull;

import com.jr.disciplinify.core.commands.CommandRunner;
import com.jr.disciplinify.core.database.Database;
import com.jr.disciplinify.core.database.DatabaseOpener;
import com.jr.disciplinify.core.database.JdbcDatabase;
import com.jr.disciplinify.core.database.MigrationHelper;
import com.jr.disciplinify.core.models.HabitList;
import com.jr.disciplinify.core.models.ModelFactory;
import com.jr.disciplinify.core.models.Timestamp;
import com.jr.disciplinify.core.models.memory.MemoryModelFactory;
import com.jr.disciplinify.core.tasks.SingleThreadTaskRunner;
import com.jr.disciplinify.core.test.HabitFixtures;
import com.jr.disciplinify.core.utils.DateUtils;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.validateMockitoUsage;

@RunWith(MockitoJUnitRunner.class)
public class BaseUnitTest {

    // 8:00am, January 25th, 2015 (UTC)
    protected static final long FIXED_LOCAL_TIME = 1422172800000L;

    protected HabitList habitList;

    protected HabitFixtures fixtures;

    protected ModelFactory modelFactory;

    protected SingleThreadTaskRunner taskRunner;

    protected CommandRunner commandRunner;

    protected DatabaseOpener databaseOpener = new DatabaseOpener() {
        @Override
        public Database open(@NonNull File file) {
            try {
                return new JdbcDatabase(DriverManager.getConnection(
                        String.format("jdbc:sqlite:%s", file.getAbsolutePath())));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    };

    @Before
    public void setUp() throws Exception {
        DateUtils.setFixedLocalTime(FIXED_LOCAL_TIME);

        modelFactory = new MemoryModelFactory();
        habitList = spy(modelFactory.buildHabitList());
        fixtures = new HabitFixtures(modelFactory, habitList);
        taskRunner = new SingleThreadTaskRunner();
        commandRunner = new CommandRunner(taskRunner);
    }

    @After
    public void tearDown() throws Exception {
        validateMockitoUsage();
        DateUtils.setFixedLocalTime(null);
    }

    public long unixTime(int year, int month, int day) {
        GregorianCalendar cal = DateUtils.getStartOfTodayCalendar();
        cal.set(year, month, day, 0, 0, 0);
        return cal.getTimeInMillis();
    }

    public long unixTime(int year, int month, int day, int hour, int minute) {
        GregorianCalendar cal = DateUtils.getStartOfTodayCalendar();
        cal.set(year, month, day, hour, minute);
        return cal.getTimeInMillis();
    }

    public Timestamp timestamp(int year, int month, int day) {
        return new Timestamp(unixTime(year, month, day));
    }

    @Test
    public void nothing() {

    }

    protected Database buildMemoryDatabase() {
        try {
            Database db = new JdbcDatabase(
                    DriverManager.getConnection("jdbc:sqlite::memory:"));
            db.execute("pragma user_version=8;");
            MigrationHelper helper = new MigrationHelper(db);
            helper.migrateTo(21);
            return db;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected void copyAssetToFile(String assetPath, File dst)
            throws IOException {
        IOUtils.copy(openAsset(assetPath), new FileOutputStream(dst));
    }

    @NonNull
    protected InputStream openAsset(String assetPath) throws IOException {
        InputStream in = getClass().getResourceAsStream(assetPath);
        if (in != null) return in;

        String basePath = "uhabits-core/src/test/resources/";
        File file = new File(basePath + assetPath);
        if (file.exists() && file.canRead()) in = new FileInputStream(file);
        if (in != null) return in;

        basePath = "src/test/resources/";
        file = new File(basePath + assetPath);
        if (file.exists() && file.canRead()) in = new FileInputStream(file);
        if (in != null) return in;

        throw new IllegalStateException("asset not found: " + assetPath);
    }

    protected Database openDatabaseResource(String path) throws IOException {
        InputStream original = openAsset(path);
        File tmpDbFile = File.createTempFile("database", ".db");
        tmpDbFile.deleteOnExit();
        IOUtils.copy(original, new FileOutputStream(tmpDbFile));
        return databaseOpener.open(tmpDbFile);
    }
}
