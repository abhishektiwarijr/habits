/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.acceptance;

import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.jr.disciplinify.BaseUserInterfaceTest;
import com.jr.disciplinify.acceptance.steps.CommonSteps;
import com.jr.disciplinify.acceptance.steps.ListHabitsSteps;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LinksTest extends BaseUserInterfaceTest
{
    @Test
    public void shouldLinkToSourceCode() throws Exception
    {
        CommonSteps.launchApp();
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.ABOUT);
        CommonSteps.clickText("View source code at GitHub");
        CommonSteps.verifyOpensWebsite("https://github.com/iSoron/uhabits");
    }

    @Test
    public void shouldLinkToTranslationWebsite() throws Exception
    {
        CommonSteps.launchApp();
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.ABOUT);
        CommonSteps.clickText("Help translate this app");
        CommonSteps.verifyOpensWebsite("translate.loophabits.org");
    }

    @Test
    public void shouldLinkToHelp() throws Exception
    {
        CommonSteps.launchApp();
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.HELP);
        CommonSteps.verifyOpensWebsite("loophabits.org/faq.html");

        CommonSteps.launchApp();
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.SETTINGS);
        CommonSteps.clickText("Help & FAQ");
        CommonSteps.verifyOpensWebsite("loophabits.org/faq.html");
    }
}
