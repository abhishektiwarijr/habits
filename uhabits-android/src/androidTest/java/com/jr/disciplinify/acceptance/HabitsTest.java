/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.acceptance;

import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.jr.disciplinify.BaseUserInterfaceTest;
import com.jr.disciplinify.acceptance.steps.CommonSteps;
import com.jr.disciplinify.acceptance.steps.EditHabitSteps;
import com.jr.disciplinify.acceptance.steps.ListHabitsSteps;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class HabitsTest extends BaseUserInterfaceTest
{
    @Test
    public void shouldCreateHabit() throws Exception
    {
        CommonSteps.launchApp();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.ADD);

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.EDIT_HABIT);
        EditHabitSteps.typeName("Hello world");
        EditHabitSteps.typeQuestion("Did you say hello to the world today?");
        EditHabitSteps.pickFrequency("Every week");
        EditHabitSteps.pickColor(5);
        EditHabitSteps.clickSave();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.verifyDisplaysText("Hello world");
    }

    @Test
    public void shouldShowHabitStatistics() throws Exception
    {
        CommonSteps.launchApp();
        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.clickText("Track time");

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.SHOW_HABIT);
        CommonSteps.verifyDisplayGraphs();
    }

    @Test
    public void shouldDeleteHabit() throws Exception
    {
        CommonSteps.launchApp();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.longClickText("Track time");
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.DELETE);
        CommonSteps.clickOK();
        CommonSteps.verifyDoesNotDisplayText("Track time");
    }

    @Test
    public void shouldEditHabit() throws Exception
    {
        CommonSteps.launchApp();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.longClickText("Track time");
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.EDIT);

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.EDIT_HABIT);
        EditHabitSteps.typeName("Take a walk");
        EditHabitSteps.typeQuestion("Did you take a walk today?");
        EditHabitSteps.clickSave();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.verifyDisplaysTextInSequence("Wake up early", "Take a walk", "Meditate");
        CommonSteps.verifyDoesNotDisplayText("Track time");
    }

    @Test
    public void shouldEditHabit_fromStatisticsScreen() throws Exception
    {
        CommonSteps.launchApp();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.clickText("Track time");

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.SHOW_HABIT);
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.EDIT);

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.EDIT_HABIT);
        EditHabitSteps.typeName("Take a walk");
        EditHabitSteps.typeQuestion("Did you take a walk today?");
        EditHabitSteps.pickColor(10);
        EditHabitSteps.clickSave();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.SHOW_HABIT);
        CommonSteps.verifyDisplaysText("Take a walk");
        CommonSteps.pressBack();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.verifyDisplaysText("Take a walk");
        CommonSteps.verifyDoesNotDisplayText("Track time");
    }

    @Test
    public void shouldArchiveAndUnarchiveHabits() throws Exception
    {
        CommonSteps.launchApp();

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.longClickText("Track time");
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.ARCHIVE);
        CommonSteps.verifyDoesNotDisplayText("Track time");
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.TOGGLE_ARCHIVED);
        CommonSteps.verifyDisplaysText("Track time");

        CommonSteps.longClickText("Track time");
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.UNARCHIVE);
        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.TOGGLE_ARCHIVED);
        CommonSteps.verifyDisplaysText("Track time");
    }

    @Test
    public void shouldToggleCheckmarksAndUpdateScore() throws Exception
    {
        CommonSteps.launchApp();
        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        ListHabitsSteps.longPressCheckmarks("Wake up early", 2);
        CommonSteps.clickText("Wake up early");

        CommonSteps.verifyShowsScreen(CommonSteps.Screen.SHOW_HABIT);
        CommonSteps.verifyDisplaysText("10%");
    }

    @Test
    public void shouldHideCompleted() throws Exception
    {
        CommonSteps.launchApp();
        CommonSteps.verifyShowsScreen(CommonSteps.Screen.LIST_HABITS);
        CommonSteps.verifyDisplaysText("Track time");
        CommonSteps.verifyDisplaysText("Wake up early");

        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.TOGGLE_COMPLETED);
        CommonSteps.verifyDoesNotDisplayText("Track time");
        CommonSteps.verifyDisplaysText("Wake up early");

        ListHabitsSteps.longPressCheckmarks("Wake up early", 1);
        CommonSteps.verifyDoesNotDisplayText("Wake up early");

        ListHabitsSteps.clickMenu(ListHabitsSteps.MenuItem.TOGGLE_COMPLETED);
        CommonSteps.verifyDisplaysText("Track time");
        CommonSteps.verifyDisplaysText("Wake up early");
    }
}
