/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.acceptance;

import com.jr.disciplinify.BaseUserInterfaceTest;
import com.jr.disciplinify.acceptance.steps.CommonSteps;
import com.jr.disciplinify.acceptance.steps.WidgetSteps;

import org.junit.Test;

public class WidgetTest extends BaseUserInterfaceTest
{
    @Test
    public void shouldCreateAndToggleCheckmarkWidget() throws Exception
    {
        WidgetSteps.longPressHomeScreen();
        WidgetSteps.clickWidgets();
        WidgetSteps.scrollToHabits();
        WidgetSteps.dragWidgetToHomescreen();
        WidgetSteps.clickText("Wake up early");
        WidgetSteps.verifyCheckmarkWidgetIsShown();
        WidgetSteps.clickCheckmarkWidget();

        CommonSteps.launchApp();
        WidgetSteps.clickText("Wake up early");
        CommonSteps.verifyDisplaysText("5%");

        CommonSteps.pressHome();
        WidgetSteps.clickCheckmarkWidget();

        CommonSteps.launchApp();
        WidgetSteps.clickText("Wake up early");
        CommonSteps.verifyDisplaysText("0%");
    }
}
