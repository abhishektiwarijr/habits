/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify

import com.jr.disciplinify.activities.HabitModule
import com.jr.disciplinify.activities.HabitsActivityModule
import com.jr.disciplinify.activities.habits.list.ListHabitsModule
import dagger.Component
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock

@Module
class TestModule {
    @Provides
    fun ListHabitsBehavior() = mock(com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior::class.java)
}

@com.jr.androidbase.activities.ActivityScope
@Component(modules = arrayOf(
        com.jr.androidbase.activities.ActivityContextModule::class,
        com.jr.disciplinify.activities.about.AboutModule::class,
        HabitsActivityModule::class,
        ListHabitsModule::class,
        com.jr.disciplinify.activities.habits.show.ShowHabitModule::class,
        HabitModule::class,
        TestModule::class
), dependencies = arrayOf(com.jr.disciplinify.HabitsApplicationComponent::class))
interface HabitsActivityTestComponent {
    fun getCheckmarkPanelViewFactory(): com.jr.disciplinify.activities.habits.list.views.CheckmarkPanelViewFactory
    fun getHabitCardViewFactory(): com.jr.disciplinify.activities.habits.list.views.HabitCardViewFactory
    fun getCheckmarkButtonViewFactory(): com.jr.disciplinify.activities.habits.list.views.CheckmarkButtonViewFactory
    fun getNumberButtonViewFactory(): com.jr.disciplinify.activities.habits.list.views.NumberButtonViewFactory
    fun getNumberPanelViewFactory(): com.jr.disciplinify.activities.habits.list.views.NumberPanelViewFactory
}