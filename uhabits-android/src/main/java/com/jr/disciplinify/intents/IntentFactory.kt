/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.intents

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.auth.LoginActivity
import javax.inject.Inject

class IntentFactory
@Inject constructor() {

    fun helpTranslate(context: Context) =
            buildViewIntent(context.getString(R.string.translateURL))

    fun openDocument() = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
        addCategory(Intent.CATEGORY_OPENABLE)
        type = "*/*"
    }

    fun rateApp(context: Context) =
            buildViewIntent(context.getString(R.string.playStoreURL))

    fun sendFeedback(context: Context) =
            buildSendToIntent(context.getString(R.string.feedbackURL))

    fun startAboutActivity(context: Context) =
            Intent(context, com.jr.disciplinify.activities.about.AboutActivity::class.java)

    fun startIntroActivity(context: Context) =
            Intent(context, com.jr.disciplinify.activities.intro.IntroActivity::class.java)

    fun startLoginActivity(context: Context) =
            Intent(context, LoginActivity::class.java)

    fun startSettingsActivity(context: Context) =
            Intent(context, com.jr.disciplinify.activities.settings.SettingsActivity::class.java)

    fun startShowHabitActivity(context: Context, habit: com.jr.disciplinify.core.models.Habit) =
            Intent(context, com.jr.disciplinify.activities.habits.show.ShowHabitActivity::class.java).apply {
                data = Uri.parse(habit.uriString)
            }

    fun viewFAQ(context: Context) =
            buildViewIntent(context.getString(R.string.helpURL))

    fun viewSourceCode(context: Context) =
            buildViewIntent(context.getString(R.string.sourceCodeURL))

    private fun buildSendToIntent(url: String) = Intent().apply {
        action = Intent.ACTION_SENDTO
        data = Uri.parse(url)
    }

    private fun buildViewIntent(url: String) = Intent().apply {
        action = Intent.ACTION_VIEW
        data = Uri.parse(url)
    }
}
