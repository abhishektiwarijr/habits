/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.intents

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Context
import android.content.Intent
import android.net.Uri
import javax.inject.Inject

@com.jr.disciplinify.core.AppScope
class PendingIntentFactory
@Inject constructor(
        @com.jr.androidbase.AppContext private val context: Context,
        private val intentFactory: IntentFactory) {

    fun addCheckmark(habit: com.jr.disciplinify.core.models.Habit, timestamp: com.jr.disciplinify.core.models.Timestamp?): PendingIntent =
            PendingIntent.getBroadcast(
                    context, 1,
                    Intent(context, com.jr.disciplinify.receivers.WidgetReceiver::class.java).apply {
                        data = Uri.parse(habit.uriString)
                        action = com.jr.disciplinify.receivers.WidgetReceiver.ACTION_ADD_REPETITION
                        if (timestamp != null) putExtra("timestamp", timestamp.unixTime)
                    },
                    FLAG_UPDATE_CURRENT)

    fun dismissNotification(habit: com.jr.disciplinify.core.models.Habit): PendingIntent =
            PendingIntent.getBroadcast(
                    context, 0,
                    Intent(context, com.jr.disciplinify.receivers.ReminderReceiver::class.java).apply {
                        action = com.jr.disciplinify.receivers.WidgetReceiver.ACTION_DISMISS_REMINDER
                        data = Uri.parse(habit.uriString)
                    },
                    FLAG_UPDATE_CURRENT)

    fun removeRepetition(habit: com.jr.disciplinify.core.models.Habit): PendingIntent =
            PendingIntent.getBroadcast(
                    context, 3,
                    Intent(context, com.jr.disciplinify.receivers.WidgetReceiver::class.java).apply {
                        action = com.jr.disciplinify.receivers.WidgetReceiver.ACTION_REMOVE_REPETITION
                        data = Uri.parse(habit.uriString)
                    },
                    FLAG_UPDATE_CURRENT)

    fun showHabit(habit: com.jr.disciplinify.core.models.Habit): PendingIntent =
            android.support.v4.app.TaskStackBuilder
                    .create(context)
                    .addNextIntentWithParentStack(
                            intentFactory.startShowHabitActivity(
                                    context, habit))
                    .getPendingIntent(0, FLAG_UPDATE_CURRENT)!!

    fun showReminder(habit: com.jr.disciplinify.core.models.Habit,
                     reminderTime: Long?,
                     timestamp: Long): PendingIntent =
            PendingIntent.getBroadcast(
                    context,
                    (habit.getId()!! % Integer.MAX_VALUE).toInt() + 1,
                    Intent(context, com.jr.disciplinify.receivers.ReminderReceiver::class.java).apply {
                        action = com.jr.disciplinify.receivers.ReminderReceiver.ACTION_SHOW_REMINDER
                        data = Uri.parse(habit.uriString)
                        putExtra("timestamp", timestamp)
                        putExtra("reminderTime", reminderTime)
                    },
                    FLAG_UPDATE_CURRENT)

    fun snoozeNotification(habit: com.jr.disciplinify.core.models.Habit): PendingIntent =
            PendingIntent.getBroadcast(
                    context, 0,
                    Intent(context, com.jr.disciplinify.receivers.ReminderReceiver::class.java).apply {
                        data = Uri.parse(habit.uriString)
                        action = com.jr.disciplinify.receivers.ReminderReceiver.ACTION_SNOOZE_REMINDER
                    },
                    FLAG_UPDATE_CURRENT)

    fun toggleCheckmark(habit: com.jr.disciplinify.core.models.Habit, timestamp: Long?): PendingIntent =
            PendingIntent.getBroadcast(
                    context, 2,
                    Intent(context, com.jr.disciplinify.receivers.WidgetReceiver::class.java).apply {
                        data = Uri.parse(habit.uriString)
                        action = com.jr.disciplinify.receivers.WidgetReceiver.ACTION_TOGGLE_REPETITION
                        if (timestamp != null) putExtra("timestamp", timestamp)
                    },
                    FLAG_UPDATE_CURRENT)
}
