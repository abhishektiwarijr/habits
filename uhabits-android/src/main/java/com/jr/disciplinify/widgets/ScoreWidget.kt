/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.widgets

import android.content.Context
import android.view.View

class ScoreWidget(
        context: Context,
        id: Int,
        private val habit: com.jr.disciplinify.core.models.Habit
) : com.jr.disciplinify.widgets.BaseWidget(context, id) {

    override fun getOnClickPendingIntent(context: Context) =
            pendingIntentFactory.showHabit(habit)

    override fun refreshData(view: View) {
        val size = com.jr.disciplinify.activities.habits.show.views.ScoreCard.BUCKET_SIZES[prefs.defaultScoreSpinnerPosition]
        val scores = when (size) {
            1 -> habit.scores.toList()
            else -> habit.scores.groupBy(com.jr.disciplinify.activities.habits.show.views.ScoreCard.getTruncateField(size))
        }

        val widgetView = view as com.jr.disciplinify.widgets.views.GraphWidgetView
        (widgetView.dataView as com.jr.disciplinify.activities.common.views.ScoreChart).apply {
            setIsTransparencyEnabled(true)
            setBucketSize(size)
            setColor(com.jr.disciplinify.utils.PaletteUtils.getColor(context, habit.color))
            setScores(scores)
        }
    }

    override fun buildView() =
            com.jr.disciplinify.widgets.views.GraphWidgetView(context, com.jr.disciplinify.activities.common.views.ScoreChart(context)).apply {
                setTitle(habit.name)
            }

    override fun getDefaultHeight() = 300
    override fun getDefaultWidth() = 300
}
