/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify;

import android.content.*;

import com.jr.androidbase.AppContext;
import com.jr.androidbase.AppContextModule;
import com.jr.disciplinify.core.AppScope;
import com.jr.disciplinify.core.commands.CommandRunner;
import com.jr.disciplinify.core.commands.CreateHabitCommandFactory;
import com.jr.disciplinify.core.commands.EditHabitCommandFactory;
import com.jr.disciplinify.core.io.GenericImporter;
import com.jr.disciplinify.core.models.HabitList;
import com.jr.disciplinify.core.models.ModelFactory;
import com.jr.disciplinify.core.preferences.Preferences;
import com.jr.disciplinify.core.preferences.WidgetPreferences;
import com.jr.disciplinify.core.reminders.ReminderScheduler;
import com.jr.disciplinify.core.tasks.TaskRunner;
import com.jr.disciplinify.core.ui.NotificationTray;
import com.jr.disciplinify.core.ui.screens.habits.list.HabitCardListCache;
import com.jr.disciplinify.core.utils.MidnightTimer;
import com.jr.disciplinify.intents.IntentFactory;
import com.jr.disciplinify.intents.IntentParser;
import com.jr.disciplinify.intents.PendingIntentFactory;
import com.jr.disciplinify.receivers.ReminderController;
import com.jr.disciplinify.sync.SyncManager;
import com.jr.disciplinify.tasks.AndroidTaskRunner;
import com.jr.disciplinify.widgets.WidgetUpdater;


import dagger.*;

@AppScope
@Component(modules = {
    AppContextModule.class,
    HabitsModule.class,
    AndroidTaskRunner.class,
})
public interface HabitsApplicationComponent
{
    CommandRunner getCommandRunner();

    @AppContext
    Context getContext();

    CreateHabitCommandFactory getCreateHabitCommandFactory();

    EditHabitCommandFactory getEditHabitCommandFactory();

    GenericImporter getGenericImporter();

    HabitCardListCache getHabitCardListCache();

    HabitList getHabitList();

    HabitLogger getHabitsLogger();

    IntentFactory getIntentFactory();

    IntentParser getIntentParser();

    MidnightTimer getMidnightTimer();

    ModelFactory getModelFactory();

    NotificationTray getNotificationTray();

    PendingIntentFactory getPendingIntentFactory();

    Preferences getPreferences();

    ReminderScheduler getReminderScheduler();

    ReminderController getReminderController();

    SyncManager getSyncManager();

    TaskRunner getTaskRunner();

    WidgetPreferences getWidgetPreferences();

    WidgetUpdater getWidgetUpdater();
}
