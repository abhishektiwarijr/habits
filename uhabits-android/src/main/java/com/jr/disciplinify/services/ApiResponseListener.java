package com.jr.disciplinify.services;


public interface ApiResponseListener<T> {
    static void hello() {

    }

    void onApiSuccess(T response, String apiName);

    void onApiFailure(String failureMessage, String apiName);

    default void helloDefault() {

    }
}