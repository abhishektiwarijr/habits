package com.jr.disciplinify.services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.jr.disciplinify.BuildConfig;
import com.jr.disciplinify.HabitsApplication;
import com.jr.disciplinify.core.preferences.Preferences;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    public static final String BASE_URL;
    private static ApiClient _client = null;

    static {
        if (BuildConfig.DEBUG) {
            BASE_URL = "http://54.211.205.227:8080/disciplinify/user/";
//            BASE_URL = "http://192.168.1.210:8080/disciplinify/user/";
        } else {
            BASE_URL = "http://54.211.205.227:8080/disciplinify/user/";
        }
    }

    /**
     * Build OkHttpClient with interceptors,connection read & write timeouts.
     * Build Retrofit with baseUrl,convertFactory and client afterwards create ApiService.
     * Build Request in interceptor with header and method from chain and proceed.
     */

    String credentials = "desciplinify:Undesciplinify";
    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
    private Preferences prefManager;
    private Api _service;

    ApiClient(Context context, final boolean addHeader) {
        prefManager = ((HabitsApplication) context.getApplicationContext()).getComponent().getPreferences();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(interceptor);

        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(2 * 60 * 1000, TimeUnit.SECONDS);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();
                Request request = null;
                if (addHeader) {
                    request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .addHeader("Authorization", basic)
                            .method(original.method(), original.body())
                            .build();
                } else {
                    request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .method(original.method(), original.body())
                            .build();
                }
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        _service = retrofit.create(Api.class);
    }

    public static Api current(Context context, boolean addHeader) {
        return getInstance(context, addHeader).getService();
    }

    public static ApiClient getInstance(Context context, boolean addHeader) {
        if (_client == null) {
            _client = new ApiClient(context, addHeader);
        }
        return _client;
    }

    private Api getService() {
        return _service;
    }
}