package com.jr.disciplinify.services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.jr.disciplinify.R;

import java.util.List;

public class QuestionsAdapter extends ArrayAdapter<Questions> {
    private final List<Questions> mQList;

    public QuestionsAdapter(@NonNull Context context, int resource, List<Questions> mQList) {
        super(context, resource, mQList);
        this.mQList = mQList;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        View row;
        row = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_spinner_question, parent, false);
        TextView label = row.findViewById(R.id.text_spinner);
        label.setText(mQList.get(position).getQuestion());
        return row;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row;
        row = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_spinner, parent, false);
        TextView label = row.findViewById(R.id.spinner_questions);
        label.setText(mQList.get(position).getQuestion());
        return row;
    }
}
