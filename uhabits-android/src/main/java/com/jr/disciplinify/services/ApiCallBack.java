package com.jr.disciplinify.services;


import android.content.Context;
import android.support.annotation.NonNull;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ApiCallBack<T> implements Callback<T> {
    private ApiResponseListener<T> apiListener;
    private String apiName;
    private Context mContext;

    public ApiCallBack(ApiResponseListener<T> apiListener, String apiName, Context mContext) {
        this.apiListener = apiListener;
        this.apiName = apiName;
        this.mContext = mContext;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        switch (response.code()) {
            case 200:
                apiListener.onApiSuccess(response.body(), apiName);
                break;
            case 201:
                apiListener.onApiSuccess(response.body(), apiName);
                break;
            case 405:
                apiListener.onApiSuccess(response.body(), apiName);
                break;
            default:
                apiListener.onApiFailure(response.message(), apiName);
                break;
        }

    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        apiListener.onApiFailure(t.getMessage(), apiName);
    }
}
