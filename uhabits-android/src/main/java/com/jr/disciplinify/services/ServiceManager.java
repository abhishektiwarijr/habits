package com.jr.disciplinify.services;

import android.content.Context;

import com.google.gson.JsonObject;
import com.jr.disciplinify.HabitsApplication;
import com.jr.disciplinify.core.preferences.Preferences;

import retrofit2.Callback;

public class ServiceManager extends ApiClient {
    private Context mContext;
    private String accessToken;
    private boolean addHeader;

    public ServiceManager(Context context, boolean addHeader) {
        super(context, addHeader);
        this.mContext = context;
        this.addHeader = addHeader;
        Preferences prefManager = ((HabitsApplication) context.getApplicationContext()).getComponent().getPreferences();
//        accessToken = prefManager.getSyncKey();
    }

    public void getUserSignUpApi(Callback<ServiceResponse> callBack, ServiceRequest request) {
        current(mContext, addHeader).userSignUp(request).enqueue(callBack);
    }

    public void getUserLoginApi(Callback<ServiceResponse> callBack, ServiceRequest request) {
        current(mContext, addHeader).userLogin(request).enqueue(callBack);
    }

    public void getUserLogOutApi(Callback<ServiceResponse> callBack, ServiceRequest request) {
        current(mContext, addHeader).userLogOut(request).enqueue(callBack);
    }

    public void getUserLoginStatusApi(Callback<ServiceResponse> callBack, JsonObject request) {
        current(mContext, addHeader).userLoginStatus(request).enqueue(callBack);
    }

    public void createChargeApi(Callback<ServiceResponse> callBack, ServiceRequest request) {
        current(mContext, addHeader).createCharge(request).enqueue(callBack);
    }

    public void getUserForgotPasswordApi(Callback<ServiceResponse> callBack, String request) {
        current(mContext, addHeader).userForgotPassword(request).enqueue(callBack);
    }

    public void getSubmitAnswerApi(Callback<ServiceResponse> callBack, ServiceRequest request) {
        current(mContext, addHeader).submitAnswer(request).enqueue(callBack);
    }

    public void changePasswordApi(Callback<ServiceResponse> callBack, ServiceRequest request) {
        current(mContext, addHeader).changePassword(request).enqueue(callBack);
    }

    public void getQuestionsApi(Callback<ServiceResponse> callBack) {
        current(mContext, addHeader).getQuestions().enqueue(callBack);
    }
}