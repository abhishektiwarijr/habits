package com.jr.disciplinify.services;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {
    /*
     * api for user signup
     */
    @POST("sign-up")
    Call<ServiceResponse> userSignUp(@Body ServiceRequest serviceRequest);

    /*
     * api for user login
     */
    @POST("login")
    Call<ServiceResponse> userLogin(@Body ServiceRequest serviceRequest);

    /*
     * api for creating charge
     */
    @POST("create-charge")
    Call<ServiceResponse> createCharge(@Body ServiceRequest serviceRequest);


    /*
     * api for user login
     */
    @POST("get-login-status")
    Call<ServiceResponse> userLoginStatus(@Body JsonObject serviceRequest);

    /*
     * api for user login
     */
    @POST("logout")
    Call<ServiceResponse> userLogOut(@Body ServiceRequest serviceRequest);


    /*
     * api for user forgot password
     */
    @GET("forgot-password")
    Call<ServiceResponse> userForgotPassword(@Query("email") String email);

    /*
     * api to change password.
     */
    @POST("change-password")
    Call<ServiceResponse> changePassword(@Body ServiceRequest serviceRequest);

    /*
     * api to change password.
     */
    @POST("submit-answer")
    Call<ServiceResponse> submitAnswer(@Body ServiceRequest serviceRequest);

    /*
     * api to change password.
     */
    @GET("get-questions")
    Call<ServiceResponse> getQuestions();

}