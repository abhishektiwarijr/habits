/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities.habits.list

import android.view.Menu
import android.view.MenuItem
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.habits.list.views.HabitCardListController
import dagger.Lazy
import javax.inject.Inject

@com.jr.androidbase.activities.ActivityScope
class ListHabitsSelectionMenu @Inject constructor(
        private val screen: ListHabitsScreen,
        private val listAdapter: com.jr.disciplinify.activities.habits.list.views.HabitCardListAdapter,
        var commandRunner: com.jr.disciplinify.core.commands.CommandRunner,
        private val prefs: com.jr.disciplinify.core.preferences.Preferences,
        private val behavior: com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsSelectionMenuBehavior,
        private val listController: Lazy<HabitCardListController>,
        private val notificationTray: com.jr.disciplinify.core.ui.NotificationTray
) : com.jr.androidbase.activities.BaseSelectionMenu() {

    override fun onFinish() {
        listController.get().onSelectionFinished()
        super.onFinish()
    }

    override fun onItemClicked(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_edit_habit -> {
                behavior.onEditHabits()
                return true
            }

            R.id.action_archive_habit -> {
                behavior.onArchiveHabits()
                return true
            }

            R.id.action_unarchive_habit -> {
                behavior.onUnarchiveHabits()
                return true
            }

            R.id.action_delete -> {
                behavior.onDeleteHabits()
                return true
            }

            R.id.action_color -> {
                behavior.onChangeColor()
                return true
            }

            R.id.action_notify -> {
                for (h in listAdapter.selected)
                    notificationTray.show(h, com.jr.disciplinify.core.utils.DateUtils.getToday(), 0)
                return true
            }

            else -> return false
        }
    }

    override fun onPrepare(menu: Menu): Boolean {
        val itemEdit = menu.findItem(R.id.action_edit_habit)
        val itemColor = menu.findItem(R.id.action_color)
        val itemArchive = menu.findItem(R.id.action_archive_habit)
        val itemUnarchive = menu.findItem(R.id.action_unarchive_habit)
        val itemNotify = menu.findItem(R.id.action_notify)

        itemColor.isVisible = true
        itemEdit.isVisible = behavior.canEdit()
        itemArchive.isVisible = behavior.canArchive()
        itemUnarchive.isVisible = behavior.canUnarchive()
        setTitle(Integer.toString(listAdapter.selected.size))
        itemNotify.isVisible = prefs.isDeveloper

        return true
    }

    fun onSelectionStart() = screen.startSelection()
    fun onSelectionChange() = invalidate()
    fun onSelectionFinish() = finish()
    override fun getResourceId() = R.menu.list_habits_selection
}
