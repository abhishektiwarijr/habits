package com.jr.disciplinify.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jr.androidbase.activities.BaseActivity;
import com.jr.androidbase.activities.BaseScreen;
import com.jr.androidbase.utils.StyledResources;
import com.jr.disciplinify.R;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;
import com.stripe.android.view.StripeEditText;

public class TestActivity extends BaseActivity implements View.OnClickListener {
    public static String TOKEN = "token_";
    private Context mContext;
    private CardMultilineWidget mCardInputWidget;
    private StripeEditText mCardHolderName;
    private Button mPayButton;
    private ContentLoadingProgressBar mProgress;
    private String pk = "pk_test_lKBvcPl5lF3D9xW5MTi0pWyE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        mContext = TestActivity.this;
        mCardInputWidget = findViewById(R.id.card_input_widget);
        mCardHolderName = findViewById(R.id.card_holder_name);
        mPayButton = findViewById(R.id.btn_pay);
        mProgress = findViewById(R.id.progressBar);
        mPayButton.setOnClickListener(this);
        setupActionBarColor();
        if (getIntent().getStringExtra("pk") != null) {
            pk = getIntent().getStringExtra("pk");
        }
    }

    private void setupActionBarColor() {
        StyledResources res = new StyledResources(this);
        int color = BaseScreen.getDefaultActionBarColor(this);

        if (res.getBoolean(R.attr.useHabitColorAsPrimary))
            color = res.getColor(R.attr.aboutScreenColor);

        BaseScreen.setupActionBarColor(this, color);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    @Override
    public void onClick(View v) {
        Card cardToSave = mCardInputWidget.getCard();
        cardToSave.setName(mCardHolderName.getText().toString().trim());

        if (cardToSave == null) {
            Toast.makeText(this, "Invalid Card Data", Toast.LENGTH_SHORT).show();
        } else if (!cardToSave.validateCard()) {
            Toast.makeText(this, "Invalid Card Data", Toast.LENGTH_SHORT).show();
        } else {
            mProgress.setVisibility(View.VISIBLE);
            mProgress.show();
            Stripe stripe = new Stripe(mContext, pk);
            stripe.createToken(cardToSave, new TokenCallback() {
                        public void onSuccess(Token token) {
                            // Send token to your server
                            mProgress.hide();
                            mProgress.setVisibility(View.GONE);
                            Intent intent = new Intent();
                            intent.putExtra(TOKEN, token.getId());
                            setResult(Activity.RESULT_OK, intent);
//                            Toast.makeText(getApplicationContext(), "" + token.getId(), Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        public void onError(Exception error) {
                            // Show localized error message
                            mProgress.hide();
                            mProgress.setVisibility(View.GONE);
                            Toast.makeText(mContext, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
            );
        }
    }
}
