package com.jr.disciplinify.activities.intro

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.HabitsActivity
import com.jr.disciplinify.activities.auth.LoginActivity
import com.jr.disciplinify.activities.habits.list.ListHabitsActivity

class SplashActivity : HabitsActivity() {
    private lateinit var mRunnable: Runnable
    private lateinit var mHandler: Handler

    private lateinit var mContext: Context

    private lateinit var prefs: com.jr.disciplinify.core.preferences.Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mContext = this@SplashActivity
        prefs = appComponent.preferences
        mHandler = Handler()

        mRunnable = Runnable {
            if (prefs.isFirstRun) {
                prefs.isFirstRun = false
                goNext(com.jr.disciplinify.activities.intro.IntroActivity::class.java)
            } else {
                if (prefs.isLogin) {
                    goNext(ListHabitsActivity::class.java)
                } else goNext(LoginActivity::class.java)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mHandler.postDelayed(mRunnable, 2000.toLong())
    }

    private fun goNext(nextClass: Class<out Activity>) {
        startActivity(Intent(mContext, nextClass))
        finish()
    }

    override fun onPause() {
        mHandler.removeCallbacks(mRunnable)
        super.onPause()
    }
}