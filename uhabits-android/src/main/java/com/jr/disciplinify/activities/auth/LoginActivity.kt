package com.jr.disciplinify.activities.auth

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.HabitsActivity
import com.jr.disciplinify.activities.habits.list.ListHabitsActivity
import com.jr.disciplinify.services.MyData
import com.jr.disciplinify.utils.AppConstant
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : HabitsActivity(), View.OnClickListener, com.jr.disciplinify.services.ApiResponseListener<com.jr.disciplinify.services.ServiceResponse> {
    override fun onApiFailure(failureMessage: String?, apiName: String?) {
        hideProgress()
        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, failureMessage)
    }

    override fun onApiSuccess(response: com.jr.disciplinify.services.ServiceResponse?, apiName: String?) {
        hideProgress()
        val result = response
        when (apiName) {
            AppConstant.LOGIN -> {
                if (result != null) {
                    if (result.code == AppConstant.SUCCESS) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                        prefs.setIsLogin(true)
                        val gson = Gson()
                        val jsonInString = gson.toJson(result.data)
                        val resultBeans = gson.fromJson<MyData>(jsonInString, object : TypeToken<MyData>() {}.type)
                        prefs.userId = resultBeans.userId
                        startActivity(Intent(mContext, ListHabitsActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                        finish()
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                    }
                } else {
                    com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                }
            }
        }
    }

    private var deviceToken = ""
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.signup_text -> startActivity(Intent(this@LoginActivity, SignUpActivity::class.java))
            R.id.login -> {
                if (isValidInput()) {
                    if (com.jr.disciplinify.utils.CommonUtils.isOnline(mContext)) {
                        showProgress()
                        /*FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) {
                            p0 -> deviceToken = p0?.token.toString()
                        }*/

                        val req = com.jr.disciplinify.services.ServiceRequest()
                        req.email = email.text.toString()
                        req.password = password.text.toString()
                        req.deviceToken = FirebaseInstanceId.getInstance().token
                        req.deviceType = AppConstant.DEVICE_TYPE
//                        req.deviceKey = AppConstant.DEVICE_KEY
                        com.jr.disciplinify.services.ServiceManager(mContext, true).getUserLoginApi(com.jr.disciplinify.services.ApiCallBack(this, AppConstant.LOGIN, mContext), req)
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.internet_error))
                    }
                }
            }
            R.id.forgot_text -> {
                startActivity(Intent(this@LoginActivity, ForgotPasswordActivity::class.java))
            }
        }
    }

    private fun isValidInput(): Boolean {
        return if (email.text.isEmpty()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter email please.")
            false
        } else if (!com.jr.disciplinify.utils.CommonUtils.isValidEmail(email.text.toString())) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter a valid email please.")
            false
        } else if (password.text.isEmpty()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter password please.")
            false
        } /*else if (password.text.length < 6) {
            CommonUtils.showToast(mContext, "Password must be at least 6 characters long.")
            false
        } */ else true
    }

    private lateinit var mContext: Context

    private lateinit var prefs: com.jr.disciplinify.core.preferences.Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // set view
        setContentView(R.layout.activity_login)
        mContext = this@LoginActivity
        prefs = appComponent.preferences
        Glide.with(mContext as LoginActivity).load(R.drawable.background).into(ivBackground)
        // signup text click
        signup_text.setOnClickListener(this)
        forgot_text.setOnClickListener(this)
        login.setOnClickListener(this)

        signup_text.paintFlags = signup_text.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        forgot_text.paintFlags = forgot_text.paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }
}