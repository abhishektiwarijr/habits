/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities.intro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.jr.disciplinify.HabitsApplication;
import com.jr.disciplinify.R;
import com.jr.disciplinify.activities.auth.LoginActivity;
import com.jr.disciplinify.activities.habits.list.ListHabitsActivity;
import com.jr.disciplinify.core.preferences.Preferences;

/**
 * Activity that introduces the app to the user, shown only after the app is
 * launched for the first time.
 */
public class IntroActivity extends AppIntro2 {
    private Preferences prefs;
    private Context mContext;

    @Override
    public void init(Bundle savedInstanceState) {
        showStatusBar(false);

        prefs = ((HabitsApplication) getApplicationContext()).getComponent().getPreferences();
        mContext = IntroActivity.this;

        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_title_1),
                getString(R.string.intro_description_1), R.drawable.white_transparent,
                Color.parseColor("#194673")));

        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_title_2),
                getString(R.string.intro_description_2), R.drawable.intro_icon_2,
                Color.parseColor("#ffa726")));

        addSlide(AppIntroFragment.newInstance(getString(R.string.intro_title_4),
                getString(R.string.intro_description_4), R.drawable.intro_icon_4,
                Color.parseColor("#9575cd")));
    }

    @Override
    public void onNextPressed() {


    }

    @Override
    public void onDonePressed() {
        if (prefs.isLogin()) {
            goNext(ListHabitsActivity.class);
        } else goNext(LoginActivity.class);
    }

    private void goNext(Class<? extends Activity> nextClass) {
        startActivity(new Intent(mContext, nextClass));
        finish();
    }

    @Override
    public void onSlideChanged() {
    }
}
