/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities.habits.list

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.auth.LoginActivity
import com.jr.disciplinify.utils.AppConstant
import javax.inject.Inject

@com.jr.androidbase.activities.ActivityScope
class ListHabitsMenu @Inject constructor(
        activity: com.jr.androidbase.activities.BaseActivity,
        private val preferences: com.jr.disciplinify.core.preferences.Preferences,
        private val themeSwitcher: com.jr.disciplinify.core.ui.ThemeSwitcher,
        private val behavior: com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsMenuBehavior
) : com.jr.androidbase.activities.BaseMenu(activity), com.jr.disciplinify.services.ApiResponseListener<com.jr.disciplinify.services.ServiceResponse> {

    override fun onCreate(menu: Menu) {
        val nightModeItem = menu.findItem(R.id.actionToggleNightMode)
        val hideArchivedItem = menu.findItem(R.id.actionHideArchived)
        val hideCompletedItem = menu.findItem(R.id.actionHideCompleted)
        nightModeItem.isChecked = themeSwitcher.isNightMode
        hideArchivedItem.isChecked = !preferences.showArchived
        hideCompletedItem.isChecked = !preferences.showCompleted
    }

    override fun onApiFailure(failureMessage: String?, apiName: String?) {
        activity.hideProgress()
        com.jr.disciplinify.utils.CommonUtils.showToast(activity, failureMessage)
    }

    override fun onApiSuccess(response: com.jr.disciplinify.services.ServiceResponse?, apiName: String?) {
        activity.hideProgress()
        val result = response
        when (apiName) {
            AppConstant.LOGOUT -> {
                if (result != null) {
                    if (result.code == AppConstant.SUCCESS) {
//                        CommonUtils.showToast(activity, result.message)
                        preferences.setIsLogin(false)
                        activity.startActivity(Intent(activity, LoginActivity::class.java)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                        activity.finish()
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(activity, result.message)
                    }
                } else {
                    com.jr.disciplinify.utils.CommonUtils.showToast(activity, activity.getString(R.string.something_went_wrong))
                }
            }
        }
    }

    override fun onItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionToggleNightMode -> {
                behavior.onToggleNightMode()
                return true
            }

            R.id.actionAdd -> {
                behavior.onCreateHabit()
                return true
            }

            R.id.actionFAQ -> {
                behavior.onViewFAQ()
                return true
            }

            R.id.actionAbout -> {
                behavior.onViewAbout()
                return true
            }
            R.id.actionLogOut -> {
                if (com.jr.disciplinify.utils.CommonUtils.isOnline(activity)) {
                    activity.showProgress()
                    val req = com.jr.disciplinify.services.ServiceRequest()
                    req.userId = preferences.userId
                    com.jr.disciplinify.services.ServiceManager(activity, true).getUserLogOutApi(com.jr.disciplinify.services.ApiCallBack(this, AppConstant.LOGOUT, activity), req)
                } else {
                    com.jr.disciplinify.utils.CommonUtils.showToast(activity, activity.getString(R.string.internet_error))
                }
//                behavior.onViewLogOut()
                //TODO call log out api
                return true
            }

            R.id.actionSettings -> {
                behavior.onViewSettings()
                return true
            }

            R.id.actionHideArchived -> {
                behavior.onToggleShowArchived()
                invalidate()
                return true
            }

            R.id.actionHideCompleted -> {
                behavior.onToggleShowCompleted()
                invalidate()
                return true
            }

            R.id.actionSortColor -> {
                behavior.onSortByColor()
                return true
            }

            R.id.actionSortManual -> {
                behavior.onSortByManually()
                return true
            }

            R.id.actionSortName -> {
                behavior.onSortByName()
                return true
            }

            R.id.actionSortScore -> {
                behavior.onSortByScore()
                return true
            }

            else -> return false
        }
    }

    override fun getMenuResourceId() = R.menu.list_habits
}
