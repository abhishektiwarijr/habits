/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities.habits.list

import android.content.Context
import com.jr.disciplinify.activities.HabitsDirFinder
import dagger.Binds
import dagger.Module
import javax.inject.Inject

class BugReporterProxy
@Inject constructor(
        @com.jr.androidbase.AppContext context: Context
) : com.jr.androidbase.AndroidBugReporter(context), com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior.BugReporter

@Module
abstract class ListHabitsModule {

    @Binds
    abstract fun getAdapter(adapter: com.jr.disciplinify.activities.habits.list.views.HabitCardListAdapter): com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsMenuBehavior.Adapter

    @Binds
    abstract fun getBugReporter(proxy: BugReporterProxy): com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior.BugReporter

    @Binds
    abstract fun getMenuScreen(screen: ListHabitsScreen): com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsMenuBehavior.Screen

    @Binds
    abstract fun getScreen(screen: ListHabitsScreen): com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior.Screen

    @Binds
    abstract fun getSelMenuAdapter(adapter: com.jr.disciplinify.activities.habits.list.views.HabitCardListAdapter): com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsSelectionMenuBehavior.Adapter

    @Binds
    abstract fun getSelMenuScreen(screen: ListHabitsScreen): com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsSelectionMenuBehavior.Screen

    @Binds
    abstract fun getSystem(system: HabitsDirFinder): com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior.DirFinder
}
