/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities.habits.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.HabitsActivity
import com.jr.disciplinify.activities.TestActivity
import com.jr.disciplinify.activities.auth.LoginActivity
import com.jr.disciplinify.core.ui.ThemeSwitcher.THEME_DARK
import com.jr.disciplinify.services.ApiCallBack
import com.jr.disciplinify.services.ServiceManager
import com.jr.disciplinify.services.ServiceRequest
import com.jr.disciplinify.services.ServiceResponse
import com.jr.disciplinify.utils.AppConstant
import com.jr.disciplinify.utils.CommonUtils


class ListHabitsActivity : HabitsActivity(), com.jr.disciplinify.services.ApiResponseListener<com.jr.disciplinify.services.ServiceResponse> {
    var pureBlack: Boolean = false

    lateinit var adapter: com.jr.disciplinify.activities.habits.list.views.HabitCardListAdapter
    lateinit var rootView: ListHabitsRootView
    lateinit var screen: ListHabitsScreen
    lateinit var prefs: com.jr.disciplinify.core.preferences.Preferences
    lateinit var midnightTimer: com.jr.disciplinify.core.utils.MidnightTimer

    private lateinit var mContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this@ListHabitsActivity
        prefs = appComponent.preferences
        pureBlack = prefs.isPureBlackEnabled
        midnightTimer = appComponent.midnightTimer
        rootView = component.listHabitsRootView
        screen = component.listHabitsScreen
        adapter = component.habitCardListAdapter
        setScreen(screen)
        checkLoginStatus()
    }

    override fun onApiFailure(failureMessage: String?, apiName: String?) {
        hideProgress()
        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, failureMessage)
    }

    private var mAmount: Double = 0.0

    override fun onApiSuccess(response: ServiceResponse?, apiName: String?) {
        hideProgress()
        val result = response
        when (apiName) {
            AppConstant.LOGIN_STATUS -> {
                if (result != null) {
                    if (result.code == AppConstant.LOGIN_EXPIRES) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                        prefs.setIsLogin(false)
                        startActivity(Intent(mContext, LoginActivity::class.java)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                        finish()
                    } else if (result.code == AppConstant.PAYMENT_DATE) {
                        mAmount = result.amount
                        val gson = Gson()
                        val jsonInString = gson.toJson(result.data)
                        val resultBeans = gson.fromJson<String>(jsonInString, object : TypeToken<String>() {}.type)
                        val intent = Intent(mContext, TestActivity::class.java)
                        if (resultBeans != null) {
                            intent.putExtra("pk", resultBeans)
                        }
                        this@ListHabitsActivity.startActivityForResult(intent, 7)
                    } else {
//                        val intent = Intent(mContext, TestActivity::class.java)
//                        this@ListHabitsActivity.startActivityForResult(intent, 7)
//                        CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                    }
                } else {
                    CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                }
            }

            AppConstant.CREATE_CHARGE -> {
                if (result != null) {
                    if (result.code == AppConstant.SUCCESS) {
                        if (result.message != null)
                            CommonUtils.showToast(mContext, result.message)
                    } else {
                        if (result.message != null)
                            CommonUtils.showToast(mContext, result.message)
                        prefs.setIsLogin(false)
                        startActivity(Intent(mContext, LoginActivity::class.java)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                        finish()
                    }
                } else {
                    CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                    prefs.setIsLogin(false)
                    startActivity(Intent(mContext, LoginActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                    finish()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                //TODO send token to server
                // Set your secret key: remember to change this to your live secret key in production
                // See your keys here: https://dashboard.stripe.com/account/apikeys

//                com.stripe.Stripe.apiKey = "sk_test_xYeY3EST1yE5JRwz6isxQ71O"
                // Token is created using Checkout or Elements!
                // Get the payment token ID submitted by the form:val
                var token = ""
                if (data?.getStringExtra(TestActivity.TOKEN) != null) {
                    token = data.getStringExtra(TestActivity.TOKEN)
                }

                if (CommonUtils.isOnline(mContext)) {
                    showProgress()
                    val req = ServiceRequest()
                    req.userId = prefs.userId.replace(".0", "")
                    req.paymentToken = token
                    req.amount = mAmount
                    ServiceManager(mContext, true).createChargeApi(ApiCallBack(this, AppConstant.CREATE_CHARGE, mContext), req)
                } else {
                    CommonUtils.showToast(mContext, mContext.getString(R.string.internet_error))
                }

//                com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Payment successful.")
                // use the result to update your UI and send the payment method nonce to your server
            } else {
                // handle errors here, an exception may be available in
                CommonUtils.showToast(mContext, "Payment failed.")
                prefs.setIsLogin(false)
                startActivity(Intent(mContext, LoginActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                finish()
            }
        }
    }

    private fun checkLoginStatus() {
        if (com.jr.disciplinify.utils.CommonUtils.isOnline(mContext)) {
            showProgress()
            val req = JsonObject()
            req.addProperty("userId", prefs.userId.replace(".0", ""))
//            req.userId = prefs.userId
            ServiceManager(mContext, true).getUserLoginStatusApi(ApiCallBack(this, AppConstant.LOGIN_STATUS, mContext), req)
        } else {
            CommonUtils.showToast(mContext, mContext.getString(com.jr.disciplinify.R.string.internet_error))
        }
    }

    override fun onPause() {
        midnightTimer.onPause()
        screen.onDettached()
        adapter.cancelRefresh()
        super.onPause()
    }

    override fun onResume() {
        adapter.refresh()
        screen.onAttached()
        rootView.postInvalidate()
        midnightTimer.onResume()
        if (prefs.theme == THEME_DARK && prefs.isPureBlackEnabled != pureBlack) {
            restartWithFade(ListHabitsActivity::class.java)
        }
        super.onResume()
    }
}