package com.jr.disciplinify.activities.auth

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import com.bumptech.glide.Glide
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.HabitsActivity
import com.jr.disciplinify.activities.habits.list.ListHabitsActivity
import com.jr.disciplinify.utils.AppConstant
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : HabitsActivity(), View.OnClickListener, com.jr.disciplinify.services.ApiResponseListener<com.jr.disciplinify.services.ServiceResponse>, AdapterView.OnItemSelectedListener {
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onApiFailure(failureMessage: String?, apiName: String?) {
        hideProgress()
        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, failureMessage)
    }

    var mQList: MutableList<com.jr.disciplinify.services.Questions>? = null

    override fun onApiSuccess(response: com.jr.disciplinify.services.ServiceResponse?, apiName: String?) {
        hideProgress()
        val result = response
        when (apiName) {
            AppConstant.GET_QUESTIONS -> {
                if (result != null) {
                    if (result.code == AppConstant.SUCCESS) {
                        if (result.questions.size > 0) {
                            mQList?.add(com.jr.disciplinify.services.Questions("-1", "Select security question"))
                            mQList?.addAll(result.questions)
                        } else {
                            mQList?.add(com.jr.disciplinify.services.Questions("-1", "Select security question"))
                        }
                        mAdapter.notifyDataSetChanged()
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                    }
                } else {
                    com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                }
            }
            AppConstant.SIGN_UP -> {
                if (result != null) {
                    if (result.code == AppConstant.SUCCESS) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                        prefs.setIsLogin(true)
                        startActivity(Intent(mContext, ListHabitsActivity::class.java))
                        finish()
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                    }
                } else {
                    com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.login_text -> startActivity(Intent(this@SignUpActivity, LoginActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            R.id.btn_signup -> {
                if (isValidInput()) {
                    if (com.jr.disciplinify.utils.CommonUtils.isOnline(mContext)) {
                        showProgress()
                        val req = com.jr.disciplinify.services.ServiceRequest()
                        req.name = et_name.text.toString()
                        req.email = email.text.toString()
                        req.password = password2.text.toString()
                        req.phone = phone_number.text.toString()
                        val pos = spinner_questions.selectedItemPosition
                        req.questionId = mQList?.get(pos)?.questionId
                        req.answer = answer.text.toString()
                        com.jr.disciplinify.services.ServiceManager(mContext, true).getUserSignUpApi(com.jr.disciplinify.services.ApiCallBack(this, AppConstant.SIGN_UP, mContext), req)
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.internet_error))
                    }
                }
            }
        }
    }

    private lateinit var mContext: Context

    private lateinit var mAdapter: com.jr.disciplinify.services.QuestionsAdapter

    private lateinit var prefs: com.jr.disciplinify.core.preferences.Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // set view
        setContentView(R.layout.activity_signup)
        mContext = this@SignUpActivity
        prefs = appComponent.preferences
        Glide.with(this).load(R.drawable.background).into(ivBackgroundSignUp)
        // login text click
        login_text.setOnClickListener(this)
        login_text.paintFlags = login_text.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        btn_signup.setOnClickListener(this)

        spinner_questions.onItemSelectedListener = this

        mQList = mutableListOf()

        mAdapter = com.jr.disciplinify.services.QuestionsAdapter(mContext, 0, mQList)
        spinner_questions.adapter = mAdapter

        if (com.jr.disciplinify.utils.CommonUtils.isOnline(mContext)) {
            showProgress()
            com.jr.disciplinify.services.ServiceManager(mContext, true).getQuestionsApi(com.jr.disciplinify.services.ApiCallBack(this, AppConstant.GET_QUESTIONS, mContext))
        } else {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.internet_error))
        }

    }

    private fun isValidInput(): Boolean {
        return if (et_name.text.isEmpty()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter name please.")
            false
        } else if (!com.jr.disciplinify.utils.CommonUtils.isValidFullName(et_name.text.toString())) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter valid name.")
            false
        } else if (email.text.isEmpty()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter email please.")
            false
        } else if (!com.jr.disciplinify.utils.CommonUtils.isValidEmail(email.text.toString())) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter a valid email.")
            false
        } else if (password.text.isEmpty()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter password please.")
            false
        } else if (password.text.length < 6) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Password must be at least 6 characters long.")
            false
        } else if (password2.text.isEmpty()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter confirm password please.")
            false
        } else if (password.text.toString() != password2.text.toString()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Password didn't match.")
            false
        } else if (phone_number.text.isEmpty()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter phone number.")
            false
        } else if (phone_number.text.length < 10) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter valid phone number.")
            false
        } else if (spinner_questions.selectedItemPosition == 0) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Select your security question.")
            false
        } else if (answer.text.isEmpty()) {
            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Please answer your security question.")
            false
        } else true
    }
}