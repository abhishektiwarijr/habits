/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities

import android.content.ContentUris
import android.os.Bundle
import com.jr.disciplinify.HabitsApplication

abstract class HabitsActivity : com.jr.androidbase.activities.BaseActivity() {
    lateinit var component: HabitsActivityComponent
    lateinit var appComponent: com.jr.disciplinify.HabitsApplicationComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appComponent = (applicationContext as HabitsApplication).component

        val habit = getHabitFromIntent(appComponent.habitList)
                ?: appComponent.modelFactory.buildHabit()

        component = com.jr.disciplinify.activities.DaggerHabitsActivityComponent
                .builder()
                .activityContextModule(com.jr.androidbase.activities.ActivityContextModule(this))
                .baseActivityModule(com.jr.androidbase.activities.BaseActivityModule(this))
                .habitModule(HabitModule(habit))
                .habitsApplicationComponent(appComponent)
                .build()

        component.themeSwitcher.apply()
    }

    private fun getHabitFromIntent(habitList: com.jr.disciplinify.core.models.HabitList): com.jr.disciplinify.core.models.Habit? {
        val data = intent.data ?: return null
        val habit = habitList.getById(ContentUris.parseId(data))
                ?: throw RuntimeException("habit not found")
        return habit
    }
}
