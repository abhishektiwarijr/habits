package com.jr.disciplinify.activities.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.HabitsActivity
import com.jr.disciplinify.services.MyData
import com.jr.disciplinify.utils.AppConstant
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : HabitsActivity(), View.OnClickListener, com.jr.disciplinify.services.ApiResponseListener<com.jr.disciplinify.services.ServiceResponse> {
    private var userId: String = ""

    override fun onApiSuccess(response: com.jr.disciplinify.services.ServiceResponse?, apiName: String?) {
        hideProgress()
        val result = response
        when (apiName) {
            AppConstant.FORGOT_PASSWORD -> {
                if (result != null) {
                    if (result.code == AppConstant.SUCCESS) {
                        val gson = Gson()
                        val jsonInString = gson.toJson(result.data)
                        val resultBeans = gson.fromJson<MyData>(jsonInString, object : TypeToken<MyData>() {}.type)

                        questionLabel.setText(resultBeans.question)
                        userId = resultBeans.userId
                        prefs.userId = userId
                        question.visibility = View.VISIBLE
                        answer.visibility = View.VISIBLE
                        btn_submit.tag = 2
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                    }
                } else {
                    com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                }
            }
            AppConstant.SUBMIT_ANSWER -> {
                if (result != null) {
                    if (result.code == AppConstant.SUCCESS) {
                        startActivity(Intent(mContext, ResetPasswordActivity::class.java))
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                    }
                } else {
                    com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                }
            }
        }
    }

    override fun onApiFailure(failureMessage: String?, apiName: String?) {
        hideProgress()
        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, failureMessage)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_submit -> {
                if (btn_submit.tag == 1) {
                    if (email.text.isEmpty()) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter email please.")
                    } else if (!com.jr.disciplinify.utils.CommonUtils.isValidEmail(email.text.toString())) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter a valid email.")
                    } else {
                        if (com.jr.disciplinify.utils.CommonUtils.isOnline(mContext)) {
                            showProgress()
                            com.jr.disciplinify.services.ServiceManager(mContext, true).getUserForgotPasswordApi(com.jr.disciplinify.services.ApiCallBack(this
                                    , AppConstant.FORGOT_PASSWORD, mContext), email.text.toString())
                        } else {
                            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.internet_error))
                        }
                    }
                } else {
                    if (email.text.isEmpty()) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter email please.")
                    } else if (!com.jr.disciplinify.utils.CommonUtils.isValidEmail(email.text.toString())) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter a valid email.")
                    } else if (answer.text.isEmpty()) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Answer your security question.")
                    } else {
                        if (com.jr.disciplinify.utils.CommonUtils.isOnline(mContext)) {
                            showProgress()
                            val req = com.jr.disciplinify.services.ServiceRequest()
                            req.userId = userId
                            req.answer = answer.text.toString()
                            com.jr.disciplinify.services.ServiceManager(mContext, true).getSubmitAnswerApi(com.jr.disciplinify.services.ApiCallBack(this
                                    , AppConstant.SUBMIT_ANSWER, mContext), req)
                        } else {
                            com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.internet_error))
                        }
                    }
                }
            }
        }
    }

    private lateinit var mContext: Context

    private lateinit var prefs: com.jr.disciplinify.core.preferences.Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        mContext = this@ForgotPasswordActivity
        Glide.with(this).load(R.drawable.background).into(ivBackgroundSignUp)

        prefs = appComponent.preferences
        btn_submit.tag = 1
        btn_submit.setOnClickListener(this)
    }
}
