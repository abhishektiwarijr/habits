/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities

import com.jr.disciplinify.activities.habits.list.*
import dagger.Component

@com.jr.androidbase.activities.ActivityScope
@Component(modules = arrayOf(
        com.jr.androidbase.activities.ActivityContextModule::class,
        com.jr.androidbase.activities.BaseActivityModule::class,
        com.jr.disciplinify.activities.about.AboutModule::class,
        HabitsActivityModule::class,
        ListHabitsModule::class,
        com.jr.disciplinify.activities.habits.show.ShowHabitModule::class,
        HabitModule::class
), dependencies = arrayOf(com.jr.disciplinify.HabitsApplicationComponent::class))
interface HabitsActivityComponent {
    val aboutRootView: com.jr.disciplinify.activities.about.AboutRootView
    val aboutScreen: com.jr.disciplinify.activities.about.AboutScreen
    val colorPickerDialogFactory: com.jr.disciplinify.activities.common.dialogs.ColorPickerDialogFactory
    val habitCardListAdapter: com.jr.disciplinify.activities.habits.list.views.HabitCardListAdapter
    val listHabitsBehavior: com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior
    val listHabitsMenu: ListHabitsMenu
    val listHabitsRootView: ListHabitsRootView
    val listHabitsScreen: ListHabitsScreen
    val listHabitsSelectionMenu: ListHabitsSelectionMenu
    val showHabitScreen: com.jr.disciplinify.activities.habits.show.ShowHabitScreen
    val themeSwitcher: com.jr.disciplinify.core.ui.ThemeSwitcher
}
