/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities.habits.show;

import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;

import com.jr.androidbase.activities.ActivityScope;
import com.jr.androidbase.activities.BaseActivity;
import com.jr.androidbase.activities.BaseMenu;
import com.jr.disciplinify.R;
import com.jr.disciplinify.core.preferences.Preferences;
import com.jr.disciplinify.core.ui.screens.habits.show.ShowHabitMenuBehavior;

import javax.inject.Inject;

import dagger.Lazy;

@ActivityScope
public class ShowHabitsMenu extends BaseMenu {
    @NonNull
    private final Preferences prefs;
    @NonNull
    private Lazy<ShowHabitMenuBehavior> behavior;

    @Inject
    public ShowHabitsMenu(@NonNull BaseActivity activity,
                          @NonNull Lazy<ShowHabitMenuBehavior> behavior,
                          @NonNull Preferences prefs) {
        super(activity);
        this.behavior = behavior;
        this.prefs = prefs;
    }

    @Override
    public void onCreate(@NonNull Menu menu) {
        super.onCreate(menu);

        if (prefs.isDeveloper())
            menu.findItem(R.id.action_randomize).setVisible(true);
    }

    @Override
    public boolean onItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_habit:
                behavior.get().onEditHabit();
                return true;

            case R.id.export:
                behavior.get().onExportCSV();
                return true;

            case R.id.action_delete:
                behavior.get().onDeleteHabit();
                return true;

            case R.id.action_randomize:
                behavior.get().onRandomize();
                return true;

            default:
                return false;
        }
    }

    @Override
    protected int getMenuResourceId() {
        return R.menu.show_habit;
    }
}
