/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.activities.habits.list

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.support.annotation.StringRes
import android.widget.Toast
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.common.dialogs.NumberPickerFactory
import com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior.Message.*
import com.jr.disciplinify.intents.IntentFactory
import com.jr.disciplinify.tasks.ImportDataTask
import dagger.Lazy
import java.io.File
import java.io.IOException
import javax.inject.Inject

const val RESULT_IMPORT_DATA = 1
const val RESULT_EXPORT_CSV = 2
const val RESULT_EXPORT_DB = 3
const val RESULT_BUG_REPORT = 4
const val RESULT_REPAIR_DB = 5
const val REQUEST_OPEN_DOCUMENT = 6
const val REQUEST_SETTINGS = 7

@com.jr.androidbase.activities.ActivityScope
class ListHabitsScreen
@Inject constructor(
        activity: com.jr.androidbase.activities.BaseActivity,
        rootView: ListHabitsRootView,
        private val commandRunner: com.jr.disciplinify.core.commands.CommandRunner,
        private val intentFactory: IntentFactory,
        private val themeSwitcher: com.jr.disciplinify.core.ui.ThemeSwitcher,
        private val preferences: com.jr.disciplinify.core.preferences.Preferences,
        private val adapter: com.jr.disciplinify.activities.habits.list.views.HabitCardListAdapter,
        private val taskRunner: com.jr.disciplinify.core.tasks.TaskRunner,
        private val exportDBFactory: com.jr.disciplinify.tasks.ExportDBTaskFactory,
        private val importTaskFactory: com.jr.disciplinify.tasks.ImportDataTaskFactory,
        private val confirmDeleteDialogFactory: com.jr.disciplinify.activities.common.dialogs.ConfirmDeleteDialogFactory,
        private val colorPickerFactory: com.jr.disciplinify.activities.common.dialogs.ColorPickerDialogFactory,
        private val editHabitDialogFactory: com.jr.disciplinify.activities.habits.edit.EditHabitDialogFactory,
        private val numberPickerFactory: NumberPickerFactory,
        private val behavior: Lazy<com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior>,
        private val menu: Lazy<ListHabitsMenu>,
        private val selectionMenu: Lazy<ListHabitsSelectionMenu>
) : com.jr.androidbase.activities.BaseScreen(activity),
        com.jr.disciplinify.core.commands.CommandRunner.Listener,
        com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior.Screen,
        com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsMenuBehavior.Screen,
        com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsSelectionMenuBehavior.Screen {

    override fun showAuthScreen() {
        val intent = intentFactory.startLoginActivity(activity)
        activity.startActivity(intent)
    }

    init {
        setRootView(rootView)
    }

    fun onAttached() {
        setMenu(menu.get())
        setSelectionMenu(selectionMenu.get())
        commandRunner.addListener(this)
    }

    fun onDettached() {
        commandRunner.removeListener(this)
    }

    override fun onCommandExecuted(command: com.jr.disciplinify.core.commands.Command, refreshKey: Long?) {
        if (command.isRemote) return
        showMessage(getExecuteString(command))
    }

    override fun onResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_OPEN_DOCUMENT -> onOpenDocumentResult(resultCode, data)
            REQUEST_SETTINGS -> onSettingsResult(resultCode)
        }
    }

    private fun onOpenDocumentResult(resultCode: Int, data: Intent?) {
        if (data == null) return
        if (resultCode != Activity.RESULT_OK) return
        try {
            val inStream = activity.contentResolver.openInputStream(data.data)
            val cacheDir = activity.externalCacheDir
            val tempFile = File.createTempFile("import", "", cacheDir)
            com.jr.androidbase.utils.FileUtils.copy(inStream, tempFile)
            onImportData(tempFile) { tempFile.delete() }
        } catch (e: IOException) {
            showMessage(R.string.could_not_import)
            e.printStackTrace()
        }
    }

    private fun onSettingsResult(resultCode: Int) {
        when (resultCode) {
            RESULT_IMPORT_DATA -> showImportScreen()
            RESULT_EXPORT_CSV -> behavior.get().onExportCSV()
            RESULT_EXPORT_DB -> onExportDB()
            RESULT_BUG_REPORT -> behavior.get().onSendBugReport()
            RESULT_REPAIR_DB -> behavior.get().onRepairDB()
        }
    }

    override fun applyTheme() {
        themeSwitcher.apply()
        activity.restartWithFade(ListHabitsActivity::class.java)
    }

    override fun showAboutScreen() {
//        Toast.makeText(activity, "Coming soon", Toast.LENGTH_SHORT).show()
        val intent = intentFactory.startAboutActivity(activity)
        activity.startActivity(intent)
    }

    fun showCreateBooleanHabitScreen() {
        val dialog = editHabitDialogFactory.createBoolean()
        activity.showDialog(dialog, "editHabit")
    }

    override fun showCreateHabitScreen() {
        if (!preferences.isNumericalHabitsFeatureEnabled) {
            showCreateBooleanHabitScreen()
            return
        }

        val dialog = AlertDialog.Builder(activity)
                .setTitle("Type of habit")
                .setItems(R.array.habitTypes) { _, which ->
                    if (which == 0) showCreateBooleanHabitScreen()
                    else showCreateNumericalHabitScreen()
                }
                .create()

        dialog.show()
    }

    override fun showDeleteConfirmationScreen(callback: com.jr.disciplinify.core.ui.callbacks.OnConfirmedCallback) {
        activity.showDialog(confirmDeleteDialogFactory.create(callback))
    }

    override fun showEditHabitsScreen(habits: List<com.jr.disciplinify.core.models.Habit>) {
        val dialog = editHabitDialogFactory.edit(habits[0])
        activity.showDialog(dialog, "editNumericalHabit")
    }

    override fun showFAQScreen() {
        Toast.makeText(activity, "Coming soon", Toast.LENGTH_SHORT).show()
//        val intent = intentFactory.viewFAQ(activity)
//        activity.startActivity(intent)
    }

    override fun showHabitScreen(habit: com.jr.disciplinify.core.models.Habit) {
        val intent = intentFactory.startShowHabitActivity(activity, habit)
        activity.startActivity(intent)
    }

    fun showImportScreen() {
        val intent = intentFactory.openDocument()
        activity.startActivityForResult(intent, REQUEST_OPEN_DOCUMENT)
    }


    override fun showIntroScreen() {
        val intent = intentFactory.startIntroActivity(activity)
        activity.startActivity(intent)
    }

    override fun showMessage(m: com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior.Message) {
        showMessage(when (m) {
            COULD_NOT_EXPORT -> R.string.could_not_export
            IMPORT_SUCCESSFUL -> R.string.habits_imported
            IMPORT_FAILED -> R.string.could_not_import
            DATABASE_REPAIRED -> R.string.database_repaired
            COULD_NOT_GENERATE_BUG_REPORT -> R.string.bug_report_failed
            FILE_NOT_RECOGNIZED -> R.string.file_not_recognized
        })
    }

    override fun showSendBugReportToDeveloperScreen(log: String) {
        val to = R.string.bugReportTo
        val subject = R.string.bugReportSubject
        showSendEmailScreen(to, subject, log)
    }

    override fun showSettingsScreen() {
        val intent = intentFactory.startSettingsActivity(activity)
        activity.startActivityForResult(intent, REQUEST_SETTINGS)
    }

    override fun showColorPicker(defaultColor: Int,
                                 callback: com.jr.disciplinify.core.ui.callbacks.OnColorPickedCallback) {
        val picker = colorPickerFactory.create(defaultColor)
        picker.setListener(callback)
        activity.showDialog(picker, "picker")
    }

    override fun showNumberPicker(value: Double,
                                  unit: String,
                                  callback: com.jr.disciplinify.core.ui.screens.habits.list.ListHabitsBehavior.NumberPickerCallback) {
        numberPickerFactory.create(value, unit, callback).show()
    }

    @StringRes
    private fun getExecuteString(command: com.jr.disciplinify.core.commands.Command): Int? {
        when (command) {
            is com.jr.disciplinify.core.commands.ArchiveHabitsCommand -> return R.string.toast_habit_archived
            is com.jr.disciplinify.core.commands.ChangeHabitColorCommand -> return R.string.toast_habit_changed
            is com.jr.disciplinify.core.commands.CreateHabitCommand -> return R.string.toast_habit_created
            is com.jr.disciplinify.core.commands.DeleteHabitsCommand -> return R.string.toast_habit_deleted
            is com.jr.disciplinify.core.commands.EditHabitCommand -> return R.string.toast_habit_changed
            is com.jr.disciplinify.core.commands.UnarchiveHabitsCommand -> return R.string.toast_habit_unarchived
            else -> return null
        }
    }

    private fun showCreateNumericalHabitScreen() {
        val dialog = editHabitDialogFactory.createNumerical()
        activity.showDialog(dialog, "editHabit")
    }

    private fun onImportData(file: File, onFinished: () -> Unit) {
        taskRunner.execute(importTaskFactory.create(file) { result ->
            if (result == com.jr.disciplinify.tasks.ImportDataTask.SUCCESS) {
                adapter.refresh()
                showMessage(R.string.habits_imported)
            } else if (result == com.jr.disciplinify.tasks.ImportDataTask.NOT_RECOGNIZED) {
                showMessage(R.string.file_not_recognized)
            } else {
                showMessage(R.string.could_not_import)
            }
            onFinished()
        })
    }

    private fun onExportDB() {
        taskRunner.execute(exportDBFactory.create { filename ->
            if (filename != null) showSendFileScreen(filename)
            else showMessage(R.string.could_not_export)
        })
    }
}
