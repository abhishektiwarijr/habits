package com.jr.disciplinify.activities.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.jr.disciplinify.R
import com.jr.disciplinify.activities.HabitsActivity
import com.jr.disciplinify.utils.AppConstant
import kotlinx.android.synthetic.main.activity_reset_password.*

class ResetPasswordActivity : HabitsActivity(), View.OnClickListener, com.jr.disciplinify.services.ApiResponseListener<com.jr.disciplinify.services.ServiceResponse> {
    override fun onApiSuccess(response: com.jr.disciplinify.services.ServiceResponse?, apiName: String?) {
        hideProgress()
        val result = response
        when (apiName) {
            AppConstant.CHANGE_PASSWORD -> {
                if (result != null) {
                    if (result.code == AppConstant.SUCCESS) {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                        startActivity(Intent(mContext, LoginActivity::class.java)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                        finish()
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, result.message)
                    }
                } else {
                    com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.something_went_wrong))
                }
            }
        }
    }

    override fun onApiFailure(failureMessage: String?, apiName: String?) {
        hideProgress()
        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, failureMessage)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_submit -> {
                if (isValidInput()) {
                    if (com.jr.disciplinify.utils.CommonUtils.isOnline(mContext)) {
                        showProgress()
                        val req = com.jr.disciplinify.services.ServiceRequest()
                        req.userId = pref.userId
                        req.password = password2.text.toString()
                        com.jr.disciplinify.services.ServiceManager(mContext, true).changePasswordApi(com.jr.disciplinify.services.ApiCallBack(this
                                , AppConstant.CHANGE_PASSWORD, mContext), req)
                    } else {
                        com.jr.disciplinify.utils.CommonUtils.showToast(mContext, getString(R.string.internet_error))
                    }
                }
            }
        }
    }

    private lateinit var mContext: Context

    private lateinit var pref: com.jr.disciplinify.core.preferences.Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        mContext = this@ResetPasswordActivity
        Glide.with(this).load(R.drawable.background).into(ivBackgroundSignUp)

        pref = appComponent.preferences
        btn_submit.setOnClickListener(this)
    }

    private fun isValidInput(): Boolean {
        return when {
            password.text.isEmpty() -> {
                com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter password please.")
                false
            }
            password.text.length < 6 -> {
                com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Password must be at least 6 characters long.")
                false
            }
            password2.text.isEmpty() -> {
                com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Enter confirm password please.")
                false
            }
            password.text.toString() != password2.text.toString() -> {
                com.jr.disciplinify.utils.CommonUtils.showToast(mContext, "Password didn't match.")
                false
            }
            else -> true
        }
    }
}
