/*
 * Copyright (C) 2016 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify

import android.content.Context
import android.support.multidex.MultiDexApplication
import com.jr.disciplinify.widgets.WidgetUpdater
import java.io.File

/**
 * The Android application for Disciplinify.
 */
class HabitsApplication : MultiDexApplication() {

    private lateinit var context: Context
    private lateinit var widgetUpdater: WidgetUpdater
    private lateinit var reminderScheduler: com.jr.disciplinify.core.reminders.ReminderScheduler
    private lateinit var notificationTray: com.jr.disciplinify.core.ui.NotificationTray

    override fun onCreate() {
        super.onCreate()
        context = this
        HabitsApplication.component = com.jr.disciplinify.DaggerHabitsApplicationComponent
                .builder()
                .appContextModule(com.jr.androidbase.AppContextModule(context))
                .build()

        if (isTestMode()) {
            val db = com.jr.disciplinify.utils.DatabaseUtils.getDatabaseFile(context)
            if (db.exists()) db.delete()
        }

        try {
            com.jr.disciplinify.utils.DatabaseUtils.initializeDatabase(context)
        } catch (e: com.jr.disciplinify.core.database.UnsupportedDatabaseVersionException) {
            val db = com.jr.disciplinify.utils.DatabaseUtils.getDatabaseFile(context)
            db.renameTo(File(db.absolutePath + ".invalid"))
            com.jr.disciplinify.utils.DatabaseUtils.initializeDatabase(context)
        }

        widgetUpdater = component.widgetUpdater
        widgetUpdater.startListening()

        reminderScheduler = component.reminderScheduler
        reminderScheduler.startListening()

        notificationTray = component.notificationTray
        notificationTray.startListening()

        val prefs = component.preferences
        prefs.setLastAppVersion(com.jr.disciplinify.BuildConfig.VERSION_CODE)

        val taskRunner = component.taskRunner
        taskRunner.execute {
            reminderScheduler.scheduleAll()
            widgetUpdater.updateWidgets()
        }
    }

    override fun onTerminate() {
        reminderScheduler.stopListening()
        widgetUpdater.stopListening()
        notificationTray.stopListening()
        super.onTerminate()
    }

    val component: com.jr.disciplinify.HabitsApplicationComponent
        get() = HabitsApplication.component

    companion object {
        lateinit var component: com.jr.disciplinify.HabitsApplicationComponent

        fun isTestMode(): Boolean {
            try {
                Class.forName("com.jr.disciplinify.BaseAndroidTest")
                return true
            } catch (e: ClassNotFoundException) {
                return false
            }
        }
    }
}
