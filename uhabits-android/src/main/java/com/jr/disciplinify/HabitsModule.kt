/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify

import com.jr.disciplinify.database.AndroidDatabase
import com.jr.disciplinify.database.AndroidDatabaseOpener
import com.jr.disciplinify.intents.IntentScheduler
import com.jr.disciplinify.notifications.AndroidNotificationTray
import com.jr.disciplinify.preferences.SharedPreferencesStorage
import com.jr.disciplinify.utils.DatabaseUtils
import dagger.Module
import dagger.Provides

@Module
class HabitsModule {
    @Provides
    @com.jr.disciplinify.core.AppScope
    fun getPreferences(storage: SharedPreferencesStorage): com.jr.disciplinify.core.preferences.Preferences {
        return com.jr.disciplinify.core.preferences.Preferences(storage)
    }

    @Provides
    @com.jr.disciplinify.core.AppScope
    fun getReminderScheduler(
            sys: IntentScheduler,
            commandRunner: com.jr.disciplinify.core.commands.CommandRunner,
            habitList: com.jr.disciplinify.core.models.HabitList
    ): com.jr.disciplinify.core.reminders.ReminderScheduler {
        return com.jr.disciplinify.core.reminders.ReminderScheduler(commandRunner, habitList, sys)
    }

    @Provides
    @com.jr.disciplinify.core.AppScope
    fun getTray(
            taskRunner: com.jr.disciplinify.core.tasks.TaskRunner,
            commandRunner: com.jr.disciplinify.core.commands.CommandRunner,
            preferences: com.jr.disciplinify.core.preferences.Preferences,
            screen: AndroidNotificationTray
    ): com.jr.disciplinify.core.ui.NotificationTray {
        return com.jr.disciplinify.core.ui.NotificationTray(taskRunner, commandRunner, preferences, screen)
    }

    @Provides
    @com.jr.disciplinify.core.AppScope
    fun getWidgetPreferences(
            storage: SharedPreferencesStorage
    ): com.jr.disciplinify.core.preferences.WidgetPreferences {
        return com.jr.disciplinify.core.preferences.WidgetPreferences(storage)
    }

    @Provides
    @com.jr.disciplinify.core.AppScope
    fun getModelFactory(): com.jr.disciplinify.core.models.ModelFactory {
        return com.jr.disciplinify.core.models.sqlite.SQLModelFactory(AndroidDatabase(DatabaseUtils.openDatabase()))
    }

    @Provides
    @com.jr.disciplinify.core.AppScope
    fun getHabitList(list: com.jr.disciplinify.core.models.sqlite.SQLiteHabitList): com.jr.disciplinify.core.models.HabitList {
        return list
    }

    @Provides
    @com.jr.disciplinify.core.AppScope
    fun getDatabaseOpener(opener: AndroidDatabaseOpener): com.jr.disciplinify.core.database.DatabaseOpener {
        return opener
    }
}

