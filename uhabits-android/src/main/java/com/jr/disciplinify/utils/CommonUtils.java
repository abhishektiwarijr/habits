package com.jr.disciplinify.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.jr.disciplinify.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@SuppressWarnings("ALL")
public class CommonUtils {

    public static final String DATE_FORMAT = "HH:mm a";
    public static final String DATE_TIME_FORMAT = "dd MMM yyyy ,HH:mm a";
    /**
     * Method to Enable or Disable a view
     */

    public static final float ALPHA_LIGHT = 0.60f;
    public static final float ALPHA_DARK = 1.0f;
    private static final String TAG = "";
    private static ProgressDialog dialogProgress;
    private static int value;
    private static Dialog mDialog;
    private static Toast mToast;
    private Calendar startDate;

    public static void showLog(Context context, String message) {
        Log.e(TAG, message);
    }

    public static void hide_keyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();

        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void savePreferencesBoolean(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * getDisplay matrix width
     *
     * @param context current application context
     * @return
     */
    public static int getDisplayWidth(Context context) {
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isValidEmail(final String email) {
        if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isValidFullName(final String name) {
        if (!name.matches("^[a-zA-Z]{2,}(?: [a-zA-Z]+){0,2}$")) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean emailFieldValidation(String fieldValue) {
        return !Patterns.EMAIL_ADDRESS.matcher(fieldValue).matches();
    }

    public static String getLastThreeChar(String word) {
        if (word.length() == 3) {
            return word;
        } else if (word.length() > 3) {
            return word.substring(word.length() - 3);
        } else {
            // whatever is appropriate in this case
            throw new IllegalArgumentException("word has less than 3 characters!");
        }
    }

    public static String getDate(String currentDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String returnCurrentDate = "";
        try {

            Date date = format.parse(currentDate);

            Log.e("Time Selected:", "Time   " + date.getTime());
            Log.e("Time Selected:", "Time   " + date.getDate());
            Log.e("Time Selected:", "Time   " + DateFormat.getDateInstance(DateFormat.MEDIUM).format(date));
            Log.e("Time Selected:", "Time   " + DateFormat.getDateInstance(DateFormat.SHORT).format(date));

            SimpleDateFormat postFormater = new SimpleDateFormat("MMM dd");
            returnCurrentDate = postFormater.format(date);

            Log.e("Converted Time", returnCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnCurrentDate;
    }

    public static String getCompleteDate(String currentDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String returnCurrentDate = "";
        try {

            Date date = format.parse(currentDate);

            Log.e("Time Selected:", "Time   " + date.getTime());
            Log.e("Time Selected:", "Time   " + date.getDate());
            Log.e("Time Selected:", "Time   " + DateFormat.getDateInstance(DateFormat.MEDIUM).format(date));
            Log.e("Time Selected:", "Time   " + DateFormat.getDateInstance(DateFormat.SHORT).format(date));

            SimpleDateFormat postFormater = new SimpleDateFormat("MMM dd yyyy");
            returnCurrentDate = postFormater.format(date);

            Log.e("Converted Time", returnCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnCurrentDate;
    }

    public static boolean isValidPassword(final String password) {
        if (!password.matches("(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{6,}")) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isValidPhone(final String phoneNo) {
        if (!phoneNo.matches("[9|8|7][0-9]{9}$")) {
            return false;
        } else {
            return true;
        }
    }

    public static void showOkAlert(String title, String message, final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message)

                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.dismiss();
                            }
                        });
        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Valid Contact number
     *
     * @param phoneNo
     * @return
     */
    public static boolean isValidContactNo(final String phoneNo) {
        if (!phoneNo.matches("^[279][0-9]{7}$")) {
            return false;
        } else {
            return true;
        }
    }

    public static String getTimeFromTSDate(String timeStamp) {
        if (timeStamp.length() > 0) {
            return new SimpleDateFormat("hh:mm a")
                    .format(new Date((Long.valueOf(timeStamp))));
        } else {
            return "";
        }
    }

    public static String getDateTimeFromTSDate(String timeStamp) {
        if (timeStamp.length() > 0) {
            return new SimpleDateFormat("dd/MM/yyyy, hh mm a").format(new Date((Long.valueOf(timeStamp))));
        } else {
            return "";
        }
    }

    //get window height
    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    //get window width
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static String getMonthFromTSDate(String timeStamp) {
        if (timeStamp.length() > 0) {
            return new SimpleDateFormat("MM")
                    .format(new Date((Long.valueOf(timeStamp))));
        } else {
            return "";
        }
    }

    public static String getYearFromTSDate(String timeStamp) {
        if (timeStamp.length() > 0) {
            return new SimpleDateFormat("yyyy")
                    .format(new Date((Long.valueOf(timeStamp))));
        } else {
            return "";
        }
    }

    public static String getTimeStampDate(String date_time, String format) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        formatter.setTimeZone(TimeZone.getDefault());
        Date datee;
        try {
            datee = formatter.parse(date_time);
            String timestamp = "" + datee.getTime();
            if (timestamp.length() > 10) {
                timestamp = "" + Long.parseLong(timestamp) / 1000L;
            }
            return timestamp;
        } catch (ParseException pe) {
            pe.printStackTrace();
            return "";
        }
    }

    public static String getValueFromTS(String timeStamp, @SuppressWarnings("SameParameterValue") String dateFormat) {
        try {
            if (timeStamp.length() > 0) {
                DateFormat formatter1 = new SimpleDateFormat(dateFormat, Locale.getDefault());
                return formatter1.format(new Date((Long.valueOf(timeStamp))));
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    //date picker dialog
    public static void showFromDate(final Context mContext, final TextView textView) {
        final Calendar startDate;
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        startDate = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                startDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(dateFormatter.format(startDate.getTime()));
                startDate.getTimeInMillis();
            }
        }, startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
        fromDatePickerDialog.setCancelable(true);
    }

    //date picker dialog
    public static void showToDate(final Context mContext, final TextView textView, String timeStampDate) {
        final Calendar startDate;
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        startDate = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                startDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(dateFormatter.format(startDate.getTime()));
            }
        }, startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
        fromDatePickerDialog.getDatePicker().setMinDate(Long.parseLong(timeStampDate));
        fromDatePickerDialog.setCancelable(true);
    }

    //date picker dialog for date of birth
    public static void showDateOfBirth(final Context mContext, final TextView textView) {
        final Calendar startDate;
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        startDate = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                startDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(dateFormatter.format(startDate.getTime()));
            }
        }, startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
        fromDatePickerDialog.setCancelable(true);
        fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

    }

    //date picker dialog for date of birth
    public static void showDatePicker(final Context mContext, final TextView textView) {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                final Calendar startDate = Calendar.getInstance();
                startDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(DateFormat.getDateInstance(DateFormat.LONG).format(startDate.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        fromDatePickerDialog.show();
        fromDatePickerDialog.setCancelable(true);
    }

    //date picker dialog for date of birth
    public static void showTimePicker(final Context mContext, final TextView textView) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                textView.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
        mTimePicker.setCancelable(true);
    }

    /**
     * Create a File for saving an image or video
     */
    public static File getOutputMediaFile(Context context) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        String path = context.getExternalFilesDir(null).getPath();
        File mediaStorageDir = new File(path);
        Log.e("path", path + "");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "sig_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static String imageToBase64(Bitmap bitmap) {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
        byte[] ba = bao.toByteArray();
        String base64 = Base64.encodeToString(ba, Base64.DEFAULT);
        return base64;
    }

    public final static String bitmapToString(Bitmap in) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        in.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String base64_value = "data:image/jpg;base64," + Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT).trim();
        return base64_value;
    }

    public final static Bitmap stringToBitmap(String in) {
        byte[] bytes = Base64.decode(in, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public static void showAlertWithPositiveButton(int message, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert1 = builder.create();
        TextView tv = (TextView) alert1.findViewById(android.R.id.message);
        if (tv != null)
            tv.setTextSize(20);
        alert1.show();
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

    public static Typeface getFont(Context context) {
        Typeface font = Typeface.createFromAsset(
                context.getAssets(),
                "fonts/HelveticaNeueLTCom-LtCn.ttf");
        return font;
    }

    public static int getIntPreferences(Context context, String key) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(key, 0);
    }

    public static String replaceLast(String string, String toReplace, String replacement) {
        int pos = string.lastIndexOf(toReplace);
        if (pos > -1) {
            return string.substring(0, pos)
                    + replacement
                    + string.substring(pos + toReplace.length(), string.length());
        } else {
            return string;
        }
    }

    public static void showProgressDialog(Context context) {
        if (context != null) {
            if (dialogProgress != null) {
                try {
                    dialogProgress.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            dialogProgress = new ProgressDialog(context);
            if (dialogProgress.getWindow() != null) {
                dialogProgress.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
            dialogProgress.setMessage("Please wait, while your request is being processed ..");
            if (!dialogProgress.isShowing()) {
                try {
                    dialogProgress.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialogProgress.setCancelable(false);

        }
    }

    public static void showProgressDialog(Context context, String text) {
        String message = text;
        if (context != null) {
            if (dialogProgress != null) {
                try {
                    dialogProgress.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            dialogProgress = new ProgressDialog(context);
            if (dialogProgress.getWindow() != null) {
                dialogProgress.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
            dialogProgress.setMessage(message);
            if (!dialogProgress.isShowing()) {
                try {
                    dialogProgress.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialogProgress.setCancelable(false);

        }
    }

    public static void disMissProgressDialog(Context mContext) {
        if (dialogProgress != null) {
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    public static void saveIntPreferences(Context context, String key, int value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();

    }

    public static int getPreferencesInt(Context context, String key) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(key, 0);

    }

    public static void removePreferencesString(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.apply();

    }

    public static void savePreferencesString(Context context, String key,
                                             String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();

    }

    public static boolean getPreferencesBoolean(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(key, false);
    }

    public static String getPreferencesString(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key, "");
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        return !(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable());
    }

    public static void showAlertOk(String message, Context context, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",
                        onClickListener);
        try {
            AlertDialog dialog = builder.show();
            TextView messageView = (TextView) dialog.findViewById(android.R.id.message);
            messageView.setTextSize(18);
            messageView.setGravity(Gravity.CENTER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("CommitPrefEdits")
    public static void savePreferenceInt(Context context, String key,
                                         int value) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    @SuppressLint("CommitPrefEdits")
    public static void savePreferenceString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public static void setEnableState(View view) {
        view.setAlpha(ALPHA_DARK);
        view.setEnabled(true);
        view.setClickable(true);
    }

    public static void setDisableState(View view) {
        view.setAlpha(ALPHA_LIGHT);
        view.setEnabled(false);
        view.setClickable(false);
    }

    /**
     * Method to convert Picture Path to Base64
     *
     * @param context
     * @return
     */
    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    public static String convertPathToBase64(String picturePath) {
        if (picturePath != null && picturePath.length() > 0) {
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            bitmap.recycle();
            bitmap = null;
            byte[] bytes = baos.toByteArray();
            Log.e("Common_utils", Base64.encodeToString(bytes, Base64.DEFAULT));
            return Base64.encodeToString(bytes, Base64.DEFAULT);
        }
        return "";
    }

    public static void printLog(String tag, String msg) {
        Log.e(tag, msg);
    }

    public static void setFragment(Fragment fragment, boolean removeStack, FragmentActivity activity, int mContainer, String tag) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ftTransaction = fragmentManager.beginTransaction();
        if (removeStack) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            if (tag != null)
                ftTransaction.replace(mContainer, fragment, tag);
            else
                ftTransaction.replace(mContainer, fragment);
        } else {
            if (tag != null)
                ftTransaction.replace(mContainer, fragment, tag);
            else
                ftTransaction.replace(mContainer, fragment);
            ftTransaction.addToBackStack(null);
        }
        ftTransaction.commit();
    }

    public static boolean checkPermissionStorage(Activity context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            if (result1 == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }


        } else {
            return false;

        }
    }

    public static boolean checkPermissionCamera(Activity context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }

        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * @param price
     * @return
     */
    public static String getFormatPrice(String price) {
        if (price != null && price.contains(","))
            price = price.replace(",", "");
        Double dPrice = 0.0;
        try {
            dPrice = Double.parseDouble(price);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "&#165; " + String.format(Locale.getDefault(), "%.2f", dPrice);
    }

    /**
     * @param price
     * @return
     */
    public static String getFormatPrice(float price) {
        return "&#165; " + String.format(Locale.UK, "%.2f", price);
    }

    public static String extractYoutubeId(String url) throws MalformedURLException {
        String[] param = url.split("embed/");
        String id = null;
        id = param[1];
        /*for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }*/
        return id;
    }

//    public static String extractYoutubeId(String url) throws MalformedURLException {
//        String query = new URL(url).getQuery();
//        String[] param = query.split("&");
//        String id = null;
//        for (String row : param) {
//            String[] param1 = row.split("=");
//            if (param1[0].equals("v")) {
//                id = param1[1];
//            }
//        }
//        return id;
//    }

    public static void showToast(Context mContext, String msg) {
        if (mToast != null) {
            mToast.cancel();
        }

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout = inflater.inflate(R.layout.toast_layout, null);
        TextView text = (TextView) layout.findViewById(R.id.toast);
        text.setText(msg);

        mToast = new Toast(mContext);
//        mToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setView(layout);
        mToast.show();

//        if (mToast != null) {
//            mToast.cancel();
//        }
//        mToast = Toast.makeText(mContext, msg, Toast.LENGTH_SHORT);
//        mToast.show();
    }

}