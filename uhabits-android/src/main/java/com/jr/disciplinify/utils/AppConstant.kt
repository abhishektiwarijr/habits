package com.jr.disciplinify.utils

class AppConstant {
    companion object {
        val GET_QUESTIONS = "get_questions"
        val SUCCESS = 200
        val LOGIN_EXPIRES = 407
        val PAYMENT_DATE = 402
        val SIGN_UP = "sign_up"
        val DEVICE_TYPE = "android"
        val DEVICE_KEY = "123456"
        val LOGIN = "login"
        val LOGOUT = "logout"
        val LOGIN_STATUS = "login_status"
        val CREATE_CHARGE = "create_charge"
        val FORGOT_PASSWORD = "forgot_password"
        val SUBMIT_ANSWER = "submit_answer"
        val CHANGE_PASSWORD = "change_password"
    }
}
