/*
 * Copyright (C) 2017 Álinson Santos Xavier <isoron@gmail.com>
 *
 * This file is part of Loop Habit Tracker.
 *
 * Loop Habit Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Loop Habit Tracker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.jr.disciplinify.receivers;

import com.jr.disciplinify.BaseAndroidJVMTest;
import com.jr.disciplinify.core.commands.CommandRunner;
import com.jr.disciplinify.core.models.Checkmark;
import com.jr.disciplinify.core.models.Habit;
import com.jr.disciplinify.core.models.Timestamp;
import com.jr.disciplinify.core.ui.NotificationTray;
import com.jr.disciplinify.core.ui.widgets.WidgetBehavior;
import com.jr.disciplinify.core.utils.DateUtils;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

public class WidgetControllerTest extends BaseAndroidJVMTest {
    private WidgetBehavior controller;

    private CommandRunner commandRunner;

    private Habit habit;

    private Timestamp today;

    private NotificationTray notificationTray;

    @Override
    public void setUp() {
        super.setUp();

        today = DateUtils.getToday();
        habit = fixtures.createEmptyHabit();
        commandRunner = mock(CommandRunner.class);
        notificationTray = mock(NotificationTray.class);
        controller =
                new WidgetBehavior(habitList, commandRunner, notificationTray);
    }

    @Test
    public void testOnAddRepetition_whenChecked() throws Exception {
        habit.getRepetitions().toggle(today);
        int todayValue = habit.getCheckmarks().getTodayValue();
        assertThat(todayValue, equalTo(Checkmark.CHECKED_EXPLICITLY));
        controller.onAddRepetition(habit, today);
        verifyZeroInteractions(commandRunner);
    }

    @Test
    public void testOnAddRepetition_whenUnchecked() throws Exception {
        int todayValue = habit.getCheckmarks().getTodayValue();
        assertThat(todayValue, equalTo(Checkmark.UNCHECKED));
        controller.onAddRepetition(habit, today);
        verify(commandRunner).execute(any(), isNull());
        verify(notificationTray).cancel(habit);
    }

    @Test
    public void testOnRemoveRepetition_whenChecked() throws Exception {
        habit.getRepetitions().toggle(today);
        int todayValue = habit.getCheckmarks().getTodayValue();
        assertThat(todayValue, equalTo(Checkmark.CHECKED_EXPLICITLY));
        controller.onRemoveRepetition(habit, today);
        verify(commandRunner).execute(any(), isNull());
    }

    @Test
    public void testOnRemoveRepetition_whenUnchecked() throws Exception {
        int todayValue = habit.getCheckmarks().getTodayValue();
        assertThat(todayValue, equalTo(Checkmark.UNCHECKED));
        controller.onRemoveRepetition(habit, today);
        verifyZeroInteractions(commandRunner);
    }

    @Test
    public void testOnToggleRepetition() throws Exception {
        controller.onToggleRepetition(habit, today);
        verify(commandRunner).execute(any(), isNull());
    }
}